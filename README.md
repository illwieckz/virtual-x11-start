# Virtual X11 start

```sh
./virtual-x11-start --help
```
```
virtual-x11-start: a tool to run software on virtual X11 server

Usage:

	[environment variables] virtual-x11-start [software] [software options]

Environment variables:

	MODE
		headless or windowed
		default: headless

	GEOMETRY
		<With>x<Height>
		default: 640x480

	DEPTH
		<ColorDepth>
		default: 24

Examples:

	virtual-x11-start glxinfo -B
	virtual-x11-start GEOMETRY=800x600 DEPTH=16 MODE=windowed glxgears

Requirements:

	The evilwm window manager is required.

	For headless mode, xserver-xorg and xserver-xorg-video-dummy
	are required.

	For windowed mode, xserver-xephyr is required.
	
```

Legal
-------

- Author: Thomas “illwieckz” Debesse
- License: [CC0 1.0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/legalcode)